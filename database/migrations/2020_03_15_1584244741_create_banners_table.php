<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {

            $table->integer('id', 10)->unsigned();;
            $table->string('page');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->default('NULL');
        });
    }

    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
