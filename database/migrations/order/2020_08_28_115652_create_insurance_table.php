<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInsuranceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'insurance', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->unsignedBigInteger( 'order_id' );
            $table->string( 'insurance' )->nullable();
            $table->string( 'insurance_type' )->nullable();
            $table->string( 'origin' )->nullable();
            $table->string( 'destination' )->nullable();
            $table->string( 'date_start' )->nullable();
            $table->string( 'date_end' )->nullable();
            $table->string( 'name' );
            $table->string( 'surname' );
            $table->string( 'phone' );
            $table->string( 'card_id' );
            $table->longText( 'address' );

            $table->timestamps();

            $table->foreign( 'order_id' )->references( 'id' )->on( 'orders' )->onDelete( 'cascade' );

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'insurance' );
    }
}
