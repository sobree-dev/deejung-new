<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'orders', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->unsignedBigInteger( 'user_id' );
            $table->unsignedBigInteger( 'agent_id' )->default( 0 );
            $table->string( 'code' )->unique();
            $table->decimal( 'price_summary', 8, 2 );
            $table->enum( 'state', ['PENDING', 'CHECKING', 'CONFIRMMED', 'REJECTED', 'CANCELLED', 'REFUNDED', 'EXPIRED'] )->default( 'PENDING' );
            $table->enum( 'payment_type', ['PAYPAL', 'TRANSFER'] );
            $table->string( 'slip' )->nullable();
            $table->timestamps();
            $table->softDeletes( 'deleted_at', 0 );

            $table->foreign( 'user_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );
            $table->foreign( 'agent_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'orders' );
    }
}
