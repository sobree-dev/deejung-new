<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableCommistion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table( 'transections', function ( Blueprint $table ) {
            $table->enum( 'transection_type', ['REGISTER', 'TOUR'] )->nullable()->after( 'type' );
            $table->unsignedBigInteger( 'order_id' )->nullable()->index()->after( 'agent_id' );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
