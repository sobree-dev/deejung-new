<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists( 'transections' );
        Schema::create( 'transections', function ( Blueprint $table ) {
            $table->bigIncrements( 'id' );
            $table->unsignedBigInteger( 'agent_id' );
            $table->unsignedBigInteger( 'user_id' )->nullable()->index();
            $table->longText( 'title' );
            $table->double( 'commistion', 8, 2 )->default( 0 );
            $table->double( 'withdraw', 8, 2 )->default( 0 );
            $table->enum( 'type', ['COMMISTION', 'WITHDRAW'] );
            $table->enum( 'state', ['PROCEED', 'COMPLETED', 'REJECTED'] )->nullable();
            $table->string( 'slip' )->nullable();
            $table->timestamp( 'completed_at' )->nullable();
            $table->timestamp( 'rejected_at' )->nullable();

            $table->timestamps();

            $table->foreign( 'agent_id' )->references( 'id' )->on( 'users' )->onDelete( 'cascade' );

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'transections' );
    }
}
