<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create( 'order_details', function ( Blueprint $table ) {
            $table->bigInteger( 'order_id' );
            $table->bigInteger( 'tour_id' );
            $table->integer( 'amount' );
            $table->decimal( 'price', 8, 2 );
            $table->decimal( 'price_summary', 8, 2 );
            $table->timestamps();

            $table->primary( ['order_id', 'tour_id'] );

        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists( 'order_details' );
    }
}
