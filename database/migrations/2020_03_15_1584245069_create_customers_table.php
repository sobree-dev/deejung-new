<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    public function up()
    {
        Schema::create( 'customers', function ( Blueprint $table ) {

            $table->bigIncrements( 'id' );
            $table->string( 'title' )->nullable()->default( 'NULL' );
            $table->string( 'link' )->nullable()->default( 'NULL' );
            $table->string( 'logo' )->nullable()->default( 'NULL' );
            $table->text( 'details' );
            $table->integer( 'order' )->nullable()->default( 0 );
            $table->timestamps();
            $table->softDeletes( 'deleted_at', 0 );

        } );
    }

    public function down()
    {
        Schema::dropIfExists( 'customers' );
    }
}
