<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    public function up()
    {
        Schema::create( 'services', function ( Blueprint $table ) {

            $table->bigIncrements( 'id' );
            $table->string( 'title' )->nullable()->default( 'NULL' );
            $table->string( 'slug' )->nullable()->default( 'NULL' );
            $table->text( 'excerpt' );
            $table->text( 'image' );
            $table->string( 'seo_title' )->nullable()->default( 'NULL' );
            $table->text( 'meta_description' );
            $table->text( 'meta_keywords' );
            $table->enum( 'status', ['PUBLISHED', 'DRAFT', 'PENDING'] )->default( 'DRAFT' );
            $table->longText( 'portfolio' );
            $table->datetime( 'promotion_start' )->nullable()->default( 'NULL' );
            $table->datetime( 'promotion_end' )->nullable()->default( 'NULL' );
            $table->integer( 'author_id' );
            $table->tinyInteger( 'featured' );
            $table->integer( 'order' )->nullable()->default( 0 );
            $table->string( 'price_mode', 50 )->nullable()->default( 'NULL' );
            $table->timestamps();
            $table->softDeletes( 'deleted_at', 0 );

        } );
    }

    public function down()
    {
        Schema::dropIfExists( 'services' );
    }
}
