<style>
    ol.member-list {
        list-style-type: none;
    }

    ol.member-list li {
        position: relative;
        margin: 0;
        padding-bottom: 1em;
        padding-left: 20px;
    }

    ol.member-list li:before {
        content: '';
        background-color: #c00;
        position: absolute;
        bottom: 0px;
        top: 0px;
        left: 6px;
        width: 3px;
    }

    ol.member-list li:after {
        content: '';
        background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' aria-hidden='true' viewBox='0 0 32 32' focusable='false'%3E%3Ccircle stroke='none' fill='%23c00' cx='16' cy='16' r='10'%3E%3C/circle%3E%3C/svg%3E");
        position: absolute;
        left: 0;
        top: 2px;
        height: 12px;
        width: 12px;
    }
    .card-img-top{
        width: 350px;
    }
</style>
@extends('voyager::master')
@section('page_title', __('voyager::generic.view').' '.$dataType->getTranslatedAttribute('display_name_singular'))
@section('page_header')
<h1 class="page-title">
    @php
    $dataType->icon = 'assets/media/icons/svg/Communication/Group.svg';
    $icon = '<img class="svg" src="' . asset($dataType->icon) . '" style=" width: 35px; height: 35px; position:
        absolute; left: 30px;">';
    @endphp
    {!! $icon !!} {{ 'Viewing Business Group' }}
    &nbsp;: {{ ucfirst($dataTypeContent->name) }}&nbsp;
    <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
        <span class="glyphicon glyphicon-list"></span>&nbsp;
        {{ __('voyager::generic.return_to_list') }}
    </a>
</h1>
@include('voyager::multilingual.language-selector')
@stop
@section('content')
<div class="page-content read container-fluid">
    <div class="row">
        <div class="col-md-12">
            @if($leader)
            <!-- form start -->
            <div class="card mx-auto" style="width: 18rem; margin:0 auto 20px auto">
                <div class="card-header">
                    <h3 class="panel-title text-center">{{ 'Leader' }}</h3>
                </div>
                <div class="card-body">
                    <img src="{{ Voyager::image( $leader->avatar ) }}" class=" card-img-top">
                    <h5 class="card-title">{{ $leader->name." ".$leader->name  }}</h5>
                    <p class="card-text">Affiliate Code :
                        <strong> {{ $leader->affiliate_id }} </strong>
                    </p>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <hr>
            @endif
            <div class="card" style="width: 25rem;margin:0 auto 20px auto">
                <img src="{{ filter_var($dataTypeContent->avatar, FILTER_VALIDATE_URL) ? $dataTypeContent->avatar : Voyager::image( $dataTypeContent->avatar ) }}"
                    class=" card-img-top">
                <div class="card-body">
                    <h5 class="card-title">{{ $dataTypeContent->name }}</h5>
                    <p class="card-text">Affiliate Code :
                        <strong> {{ $dataTypeContent->affiliate_id }} </strong>
                    </p>
                </div>
            </div>
            @if(sizeof($members) > 0)
            <h3 class="panel-title">{{ 'Member' }}</h3>
            <div class="row">
                @foreach ($memberUnder as $key => $item)
                <div class="col-md-6">
                    <span class="border border-info">
                        <div class="card" style="width: 18rem;margin: 0 auto 20px auto;">
                            {{--<img src="{{ filter_var($item->avatar, FILTER_VALIDATE_URL) ? $item->avatar : Voyager::image( $item->avatar ) }}"
                                class=" card-img-top">--}}
                            <div class="card-body">
                                <h5 class="card-title">{{ $item['unders']['name'] }}</h5>
                                <p class="card-text">Affiliate Code :
                                    <strong> {{ $item['unders']['affiliate'] }} </strong>
                                </p>
                            </div>
                        </div>
                        @if(sizeof($item['members']) > 0)
                        <div class="collapse in" id="member{{$key}}">
                            <ol class="member-list">
                                @foreach ($item['members'] as $member)
                                <li>
                                    <div class="panel panel-bordered">
                                        {{--<img src="{{ filter_var($member->avatar, FILTER_VALIDATE_URL) ? $member->avatar : Voyager::image( $member->avatar ) }}"
                                             class=" card-img-top">--}}
                                        <div class="panel-body">
                                            <div class="form-group">
                                                <h5 class="card-title">{{ $member->name }}</h5>
                                                <p class="card-text">Affiliate Code :
                                                    <strong> {{ $member->affiliate_id }} </strong>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ol>
                        </div>
                        @endif
                    </span>
                </div>
                @endforeach
            </div>
            @endif
        </div>
    </div>
</div>
{{-- Single delete modal --}}
@stop
@section('javascript')
@if ($isModelTranslatable)
<script>
    $(document).ready(function () {
                $('.side-body').multilingual();
            });
</script>
@endif
@stop
