@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')

@push('custom-scripts')
<link rel="stylesheet" href="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.css') }}">
@endpush
<main class="">
    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-5 text-center">
                    <h2>ข้อมูลธนาคาร</h2>
                </div>
                <div class="offset-lg-2 col-xl-8 col-lg-8 author_info text-right mb-3">
                    <a href="{{ route('profile') }}">
                        <button type="submit" class="genric-btn warning">< กลับหน้าข้อมูลส่วนตัว</button> 
                    </a>
                </div>
                <div class="offset-lg-2 col-xl-8 col-lg-8 author_info">
                    <div class="mb-3">
                        <span>** กรุณากรอกข้อมูลให้ครบถ้วน</span>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                    <form class="form-contact" action="{{ route('business.update', Auth::user()->id) }}" method="post" enctype="multipart/form-data" id="FormProfile">
                    @method('PUT')
                    <!-- CSRF TOKEN -->
                    @csrf
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">ชื่อธนาคาร</label>
                                    <input type="text" name="bank" class="form-control" value="{{ Auth::user()->bank }}"
                                        placeholder="ชื่อธนาคาร" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">ชื่อบัญชี</label>
                                    <input type="text" name="book_bank_name" class="form-control" value="{{ Auth::user()->book_bank_name }}"
                                        placeholder="ชื่อบัญชี" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">เลขที่บัญชี</label>
                                    <input type="text" name="book_bank_number" class="form-control" value="{{ Auth::user()->book_bank_number }}"
                                        placeholder="เลขที่บัญชี" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">รหัสประจำตัวประชาชน</label>
                                    <input type="text" name="id_card" class="form-control" value="{{ Auth::user()->id_card }}"
                                        placeholder="รหัสประจำตัวประชาชน" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">สำเนาประจำตัวประชาชน</label>
                                    @if (Auth::user()->file_id_card)
                                    <a href="{{ url('storage/'.Auth::user()->file_id_card) }}" target="_blank">
                                        <p class="mb-2" style="color: rgb(0, 123, 255);">คลิกดูสำเนาประจำตัวประชาชน <i class="fa fa-external-link"></i></p>
                                    </a>
                                    @endif
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="file_id_card">
                                        <label class="custom-file-label" for="">เลือกไฟล์ของคุณ</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="">สำเนาบัณชีธนาคาร</label>
                                    @if (Auth::user()->file_book_bank)
                                    <a href="{{ url('storage/'.Auth::user()->file_book_bank) }}" target="_blank">
                                        <p class="mb-2" style="color: rgb(0, 123, 255);">คลิกดูสำเนาบัณชีธนาคาร <i class="fa fa-external-link"></i></p>
                                    </a>
                                    @endif
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="file_book_bank">
                                        <label class="custom-file-label" for="">เลือกไฟล์ของคุณ</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <a href="{{ route('business.payment') }}">
                                <button type="submit" class="genric-btn success">บันทึก</button>
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@stop
@push('custom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script>
    var clipboard = new ClipboardJS('.btn-clipboard');

    clipboard.on('success', function(e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);
        alert("Coppied!")
        e.clearSelection();
    });
    clipboard.on('error', function(e) {
    console.error('Action:', e.action);
    console.error('Trigger:', e.trigger);
});
</script>

<!-- dependencies for zip mode -->
<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/zip.js/zip.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/JQL.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/typeahead.bundle.js') }}"></script>

<script src="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.js') }}"></script>
<script type="text/javascript">
    $.Thailand({
        database: "{{ asset('plugin_front/jquery.Thailand/db.json') }}",

        $district: $('[name="district"]'),
        $amphoe: $('[name="amphoe"]'),
        $province: $('[name="province"]'),
        $zipcode: $('[name="zipcode"]'),

        onDataFill: function (data) {
            console.info('Data Filled', data);
        },

        onLoad: function () {
            console.info('Autocomplete is ready!');
            $('#loader, .demo').toggle();
        }
    });

    // watch on change

    $('[name="district"]').change(function () {
        console.log('ตำบล', this.value);
    });
    $('[name="amphoe"]').change(function () {
        console.log('อำเภอ', this.value);
    });
    $('[name="province"]').change(function () {
        console.log('จังหวัด', this.value);
    });
    $('[name="zipcode"]').change(function () {
        console.log('รหัสไปรษณีย์', this.value);
    });
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
