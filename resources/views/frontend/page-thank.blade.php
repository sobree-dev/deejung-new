@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')

@push('custom-scripts')
<link rel="stylesheet" href="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.css') }}">
@endpush
<main class="">
    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3 text-center">
                    <h1 style="font-size: 80px;">THANK YOU!</h1>
                    <i class="fa fa-check c-g" style="font-size: 90px;"></i>
                    <p>ท่านทำรายการสำเร็จแล้ว</p><br>
                    <div style="max-width: 30%; margin: 0 auto" class="d-flex justify-content-between">
                        <a href="{{ route('member.order') }}" class="c-o"><u>ดูประวัติการสั่งซื้อ</u></a>
                        <a href="{{ route('home') }}" class="c-o"><u>กลับหน้าแรก</u></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@stop
@push('custom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script>
    var clipboard = new ClipboardJS('.btn-clipboard');

    clipboard.on('success', function (e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);
        alert("Coppied!")
        e.clearSelection();
    });
    clipboard.on('error', function (e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });

</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
