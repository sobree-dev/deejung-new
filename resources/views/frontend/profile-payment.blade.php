@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')

@push('custom-scripts')
<link rel="stylesheet" href="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.css') }}">
@endpush
<main class="">
    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-5 text-center">
                    <h2>สรุปการสมัครสมาชิกเป็นธุรกิจ</h2>
                </div>
                <div class="col-xl-5 col-lg-5">
                    <h4>รายละเอียดสมัคร</h3>
                    <hr>
                    <div class="courses_sidebar">
                        <div class="author_info">
                            <div class="auhor_header">
                                <div class="thumb">
                                    <img src="" alt="">
                                </div>
                                <div class="name">
                                    <h3>ข้อมูลผู้สมัคร</h3>
                                    <p>ชื่อ-นาสกุล : ซอบรี กามา</p>
                                    <p>เบอร์โทรศัพท์ : 082-8282828</p>
                                    <p>Email : sobree.017@gmail.com</p>
                                    <hr>
                                    <h3>ข้อมูลที่ชำระ</h3>
                                    <p>จำนวนเงินที่ต้องชำระ : <u style="font-size: 18px;">5,900 บาท</u></p>
                                    <br>
                                </div>
                            </div>
                            <p>
                                <a href="{{ route('profile.business') }}" id="regis_business"
                                    class="genric-btn primary circle wfull mb-3"><b>แก้ไขข้อมูล</b></a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-7 author_info">
                    <h4>ช่องทางการชำระเงิน</h3>
                    <hr>
                    <span>** ชำระเงินผ่านระบบ <i class="fa fa-paypal"></i> Paypal หรือ ทำการโอนเงินด้วยตู้ ATM</span>
                    <br><br>
                    <p>ชื่อธนาคาร : กรุงเทพ</p>
                    <p>ชื่อบัญชี : DEEJUNG SERVICE</p>
                    <p>เลขที่บัญชี : 123-4567-890</p>
                    <br>
                    <p>
                        <a href="javascript:;" class="genric-btn info circle wfull mb-3"><b>ชำระเงินด้วย <i class="fa fa-paypal"></i> Paypal</b></a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</main>
@stop
@push('custom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script>
    var clipboard = new ClipboardJS('.btn-clipboard');

    clipboard.on('success', function(e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);
        alert("Coppied!")
        e.clearSelection();
    });
    clipboard.on('error', function(e) {
    console.error('Action:', e.action);
    console.error('Trigger:', e.trigger);
});
</script>

<!-- dependencies for zip mode -->
<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/zip.js/zip.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/JQL.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/typeahead.bundle.js') }}"></script>

<script src="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.js') }}"></script>
<script type="text/javascript">
    $.Thailand({
        database: "{{ asset('plugin_front/jquery.Thailand/db.json') }}",

        $district: $('[name="district"]'),
        $amphoe: $('[name="amphoe"]'),
        $province: $('[name="province"]'),
        $zipcode: $('[name="zipcode"]'),

        onDataFill: function (data) {
            console.info('Data Filled', data);
        },

        onLoad: function () {
            console.info('Autocomplete is ready!');
            $('#loader, .demo').toggle();
        }
    });

    // watch on change

    $('[name="district"]').change(function () {
        console.log('ตำบล', this.value);
    });
    $('[name="amphoe"]').change(function () {
        console.log('อำเภอ', this.value);
    });
    $('[name="province"]').change(function () {
        console.log('จังหวัด', this.value);
    });
    $('[name="zipcode"]').change(function () {
        console.log('รหัสไปรษณีย์', this.value);
    });
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
