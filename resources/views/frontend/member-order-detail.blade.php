@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@endsection
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@endsection
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@endsection
@section('content')

@push('custom-scripts')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>
<style>
.numeric {
    background: rgb(255, 255, 255);
}
</style>
@endpush

<main class="">
    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-2">
                        <h3>รายละเอียดการสั่งซื้อ</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            <div class="row">
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-12">
                            <div >
                                <h4>รหัสการสั่งซื้อ : {{ $order->code }}</h4>
                                <p>วันที่สั่งซื้อ : {{ DateThai($order->created_at, false) }}</p>
                                <p>ช่องทางการโอน : {{ $order->payment_type }}</p>
                                <p>สถานะ : <span class="badge {{ stateColor($order->state) }}">{{ $order->state }}</span></p><br>
                            </div>
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>ชื่อรายการ</th>
                                        <th>ราคา</th>
                                        <th>QTY</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($order->details as $detail)
                                    <tr>
                                        <td><img src="{{ asset('storage/'.$detail->tour->image) }}" alt="" style="width: 130px;"></td>
                                        <td>{{ $detail->tour->title }}</td>
                                        <td>{{ number_format($detail->price, 2) }} บาท</td>
                                        <td>{{ number_format($detail->amount) }}</td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        @if($order->insurance)
                        <div class="col-12">
                            <div class="box-shadows">
                                <div class="row">
                                    <div class="col-4 mb-2">
                                        ชื่อ
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->name }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        นามสกุล
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->surname }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        เบอร์โทรศัพท์
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->phone }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        ที่อยู่
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->address }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        เลขบัตรประชาชน/หนังสือเดินทาง
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->card_id }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        ประกันภัย
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->insurance }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        แผนประกันภัย
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->insurance_type }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        ต้นทาง
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->origin }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        ปลายทาง
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->destination }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        วันเริ่มคุ่มครอง
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->date_start }}
                                    </div>

                                    <div class="col-4 mb-2">
                                        วันสินสุด
                                    </div>
                                    <div class="col-8">
                                        {{ $order->insurance->date_end }}
                                    </div>

                                </div>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
                <div class="col-md-4">
                    <form class="form-contact" action="{{ route('tours.repay', $order->id) }}" method="post" enctype="multipart/form-data">
                        @method('put')
                        @csrf
                        <div class="box-shadows">
                            <div class="text-center">
                                <h4 class="c-o">สรุปยอดสั่งซื้อ</h4>
                            </div>
                            <div>
                                <p>ชื่อ : {{ Auth::user()->name }} {{ Auth::user()->last_name }}</p>
                                <p>อีเมล : {{ Auth::user()->email }}</p>
                            </div>
                            <hr>
                            <div class="row">
                                @foreach($order->details as $detail)
                                <div class="col-6">
                                    <p>{{ number_format($detail->price, 2) }} x <span id="total">{{ number_format($detail->amount) }}</span></p>
                                </div>
                                <div class="col-6 text-right">
                                    <p><span id="price_sum">{{ number_format($detail->price_summary, 2) }}</span> บาท</p>
                                </div>
                                @endforeach
                                <div class="col-6">
                                    <p>ค่าธรรมเนียน</p>
                                </div>
                                <div class="col-6 text-right">
                                    <p>0 บาท</p>
                                </div>
                                <div class="col-6">
                                    <h4><b>รวม</b></h4>
                                </div>
                                <div class="col-6 text-right">
                                    <h5 class="c-t"><b><span id="price_sum">{{ number_format($order->price_summary, 2) }}</span> บาท</b></h5>
                                </div>
                            </div>
                            <hr>
                            <div class="text-center">
                                @if($order->state === 'CHECKING')
                                <h4 class="c-o">หลักฐานการโอนเงิน</h4>
                                <img src="{{ asset('storage/'.$order->slip) }}" alt="{{ $order->code }}" class="img-fluid">
                                @elseif($order->state === 'CONFIRMMED')
                                <i class="fa fa-check c-t" style="font-size: 90px;"></i>
                                <h5 class="c-t">สำเร็จ</h5>
                                @elseif(in_array($order->state, ['REJECTED', 'PENDING']) )
                                    <div class="row text-left">
                                        <div class="col-12 mt-2">
                                            <div class="">
                                                <label class="form-check-label"> โอนเงิน
                                                    <input type="radio" class="form-check-input" name="payment_type" value="TRANSFER" checked>
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div id="payment1" class="font-14">
                                                <p class="mt-2">ชื่อบัญชี xxxx-xxxxx</p>
                                                <p>เลขที่บัญชี xxxx-xxxxx</p>
                                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                                    <div class="form-control" data-trigger="fileinput" style="height: 40px;">
                                                        <span class="fileinput-filename"></span>
                                                    </div>
                                                    <span class="input-group-append">
                                                        <span class="input-group-text fileinput-exists" data-dismiss="fileinput">
                                                            ลบ
                                                            </span>
                                                        <span class="input-group-text btn-file">
                                                            <span class="fileinput-new">อัปโหลด</span>
                                                            <span class="fileinput-exists">เปลี่ยน</span>
                                                            <input type="file" name="slip" accept="image/*">
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12 mt-2">
                                            <div class="">
                                                <label class="form-check-label"> บัครเครดิต หรือ paypal
                                                    <input type="radio" class="form-check-input" name="payment_type" value="PAYPAL">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                            <div id="payment2" class="font-14" style="display: none;">
                                                <div class="genric-btn primary circle wfull mt-1">
                                                    <img src="{{ url('/img/paypal.png') }}" alt="" style="width: 85px;">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col">
                                            <button type="submit" class="genric-btn success circle wfull">จ่ายเงินอีกครั้ง</button>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection
@push('custom-scripts')
<script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.2.0/dist/jquery.countdown.min.js"></script>
<script>
    $('[data-countdown]').each(function () {
        var $this = $(this),
            finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function (event) {
            if (event.strftime('%-D') == 0) {
                $this.html(event.strftime('%H:%M:%S'));
            } else {
                $this.html(event.strftime('%-D วัน  %H:%M:%S'));
            }
        });
    });

</script>
<script>
    var radioValue = $("input[name='payment_type']:checked").val();
    payment_type(radioValue);
    $("input[name='payment_type']").click(function () {
        payment_type($(this).val());
    });

    function payment_type(nStr){
        if (nStr=='PAYPAL') {
            $("#payment2").css("display", "block");
            $("#payment1").css("display", "none");
        }else{
            $("#payment1").css("display", "block");
            $("#payment2").css("display", "none");
        }
    }
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@endsection
