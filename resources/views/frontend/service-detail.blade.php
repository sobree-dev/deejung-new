@extends('frontend.main')
@section('title', $service->title ?? setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@if(setting('web-seo.sharethis'))
{!! setting('web-seo.sharethis') !!}
@endif
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="mt-5">
    <div class="container">
        <!--Section: Jumbotron-->
        <section class="card blue-gradient wow fadeIn" id="intro">
            <!-- Content -->
            <img class="img-fluid" src="{{ Voyager::image($service->image) }}" alt="">
            <!-- Content -->
        </section>
        <!--Section: Jumbotron-->
        {{-- Share --}}
        @if(setting('web-seo.sharethis'))
        <section class="mt-3">
            <div class="sharethis-inline-share-buttons"></div>
        </section>
        @endif
        <hr>
        <section class="content">
            <h1 class="h3 text-left mb-3">{{ $service->title }}</h1>
            <div class="row">
                <div class="col-md-8">
                    {!! $service->body !!}
                </div>
                <div class="col-md-4">
                    @if(setting('contact.Number') || setting('contact.Line') || setting('contact.Facebook') ||
                    setting('contact.Messenger'))
                    <div class="contact-box text-center">
                        <div class="col-md-12">
                            ติดต่อสอบถาม
                        </div>
                        <div class="col-md-12">
                            @if(setting('contact.Number'))
                            <a href="tel:{{ setting('contact.Number') }}" class="btn phone">
                                <img src="{{ asset('img/phone.png') }}" alt="" srcset="">
                                <span>{{ setting('contact.Number') }}</span>
                            </a>
                            @endif
                            @if(setting('contact.Line'))
                            <a href="{{ setting('contact.Line') }}" class="btn line">
                                <img src="{{ asset('img/line.png') }}" alt="" srcset="">
                                <span>Line</span>
                            </a>
                            @endif
                            @if(setting('contact.Facebook'))
                            <a href="{{ setting('contact.Facebook') }}" target="_blank" class="btn facebook">
                                <img src="{{ asset('img/facebook.png') }}" alt="" srcset="">
                                <span>Facebook</span>
                            </a>
                            @endif
                            @if(setting('contact.Messenger'))
                            <a href="{{ setting('contact.Messenger') }}" target="_blank" class="btn messenger">
                                <img src="{{ asset('img/messege.png') }}" alt="" srcset="">
                                <span>Messenger</span>
                            </a>
                            @endif
                        </div>
                    </div>
                    @endif
                    @if($service->custom_price == 'amount')
                    @if(isset($service->price))
                    <div class="col-md-12 mt-3 text-center">
                        <p>ค่าบริการ ณ เวลานี้</p>
                        @php
                        $discount = '';
                        if(isset($service->discount) && (date('Y-m-d H:i:s') >= $service->promotion_start) &&
                        (date('Y-m-d H:i:s') <= $service->promotion_end) ){
                            $discount = 'discount';
                            }
                            @endphp
                            <h1 class="price {{ $discount }}">
                                {{number_format($service->price,0) }}
                                บ.</h1>
                            @if((date('Y-m-d H:i:s') >= $service->promotion_start) &&
                            (date('Y-m-d H:i:s') <= $service->promotion_end))
                                <h5 class="discount">{{number_format($service->discount,0) }} บ.</h5>
                                @if(isset($service->promotion_start) && (date('Y-m-d H:i:s') >=
                                $service->promotion_start))
                                @if($service->promotion_start < $service->promotion_end)
                                    <button class="btn pro">
                                        <p>หมดโปรใน</p>
                                        {{-- countdown --}}
                                        <div class="countdown">
                                            <h3 id="clock" data-datetime="{{ $service->promotion_end }}"></h3>
                                        </div>
                                    </button>
                                    @endif
                                    @endif
                                    @endif
                    </div>
                    @endif
                    @else
                    @if(isset($service->custom_price))
                    <div class="col-md-12 mt-3 ">
                        <p>
                            {!! $service->custom_price !!}
                        </p>
                    </div>
                    @endif
                    @endif
                </div>
        </section>
        <!--Section: Images-->
        @isset($service->works)
        <section class="text-center mt-5">
            <h1 class="h3 text-left mb-3">ผลงานรูป</h1>
            <!-- Grid row -->
            <div class="row">
                @php
                $images = json_decode($service->works);
                @endphp
                @foreach($images as $key => $image)
                <!-- Grid column -->
                <div class="col-lg-4 col-md-12 mb-3">
                    <div class="view overlay z-depth-1-half">
                        <a class="fancybox" rel="galery" href="{{ Voyager::image( $image ) }}">
                            <img src="{{ Voyager::image( $image ) }}" class="img-fluid" alt="">
                            <div class="mask rgba-white-light"></div>
                        </a>
                    </div>
                </div>
                <!-- Grid column -->
                @endforeach
            </div>
        </section>
        @endisset
        <!--Section: Images-->
        <!--Section: Video-->
        @isset($service->works_video)
        <section class="text-center mt-5">
            <h1 class="h3 text-left mb-3">ผลงานวีดีโอ</h1>
            <!-- Grid row -->
            <div class="row">
                @php
                $videos = json_decode($service->works_video);
                @endphp
                @foreach($videos as $key => $video)
                <!-- Grid column -->
                @php
                $embed = Embed::make($video)->parseUrl();
                @endphp
                @if($embed)
                <div class="col-lg-6 col-md-12 mb-3">
                    <div class="view overlay z-depth-1-half">
                        {!! $embed->getIframe() !!}
                        {{-- <a class="various fancybox fancybox.iframe" rel="video-galery" href="{{$video}}">
                        </a> --}}
                    </div>
                </div>
                @endif
                <!-- Grid column -->
                @endforeach
            </div>
        </section>
        @endisset
        <!--Section: Video-->
    </div>
</main>
@stop
@push('custom-scripts')
<script>
    $(".fancybox").fancybox();
    $(document).ready(function() {
    $(".various").fancybox({
            maxWidth    : 800,
            maxHeight   : 600,
            fitToView   : false,
            width       : '70%',
            height      : '70%',
            autoSize    : false,
            closeClick  : false,
            openEffect  : 'none',
            closeEffect : 'none'
        });
})
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
