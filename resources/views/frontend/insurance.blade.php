@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="">

    <!-- popular_courses_start -->
    <div class="popular_courses">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="section_title text-center mb-4">
                        <h3>{{ $topic }}</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="all_courses">
            <div class="container">
                <div class="row">
                    @foreach ($tours as $item)
                    <div class="col-xl-3 col-lg-3 col-md-4">
                        <div class="single_courses">
                            <div class="thumb">
                                <a href="{{ route('tours.show',$item->slug) }}">
                                    <img src="{{ Voyager::image($item->thumbnail('medium')) }}" alt="{{$item->title}}">
                                </a>
                            </div>
                            <div class="courses_info">
                                <div class="dotellipsis title">
                                    <h5><a href="{{ route('tours.show',$item->slug) }}">{{ $item->title }}</a></h5>
                                </div>
                                <div class="dotellipsis excerpt">
                                    <span>{{ $item->excerpt }}</span>
                                </div>
                                <div class="star_prise d-flex justify-content-between">
                                    <div class="star">
                                        @if(isset($item->discount) && (NOW() >= $item->promotion_start) && (NOW() <=
                                            $item->promotion_end))
                                            <span>โปรโมชั่น</span>
                                            <div class="countdown" data-countdown="{{ $item->promotion_end }}"></div>
                                            @endif
                                    </div>
                                    <div class="prise">
                                        @if(isset($item->discount) && (NOW() >= $item->promotion_start) && (NOW() <=
                                            $item->promotion_end))
                                            <span class="offer">{{ number_format($item->price,0) }} บาท</span>
                                            <p class="active_prise">
                                                {{ number_format($item->discount,0) }} บาท
                                            </p>
                                        @else
                                            <p class="active_prise">
                                                {{ $item->price ? number_format($item->price,0) . 'บาท' : 'ยังไม่กำหนดราคา' }}
                                            </p>
                                        @endif
                                    </div>
                                </div>
                                <hr>
                                <div class="row text-center">
                                    <div class="col-8">
                                        <a href="{{ route('insurance.show',$item->slug) }}" class="genric-btn primary circle wfull">
                                            รายละเอียด
                                        </a>
                                    </div>
                                    <div class="col-4">
                                        @guest
                                        <a href="{{ route('login') }}">
                                        @else
                                        <a href="{{ route('insurance.payment',$item->slug) }}">
                                        @endguest
                                            <img src="{{ url('/img/cart.png') }}" alt="" style="width: 40px;">
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-xl-12">
                        <div class="more_courses text-center">
                            <a href="{{ route('tours.index') }}" class="genric-btn info-border circle">ดูทั้งหมด</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- popular_courses_end-->

</main>
@stop
@push('custom-scripts')
<script src="//cdn.rawgit.com/hilios/jQuery.countdown/2.2.0/dist/jquery.countdown.min.js"></script>
<script>
    $('[data-countdown]').each(function() {
        var $this = $(this), finalDate = $(this).data('countdown');
        $this.countdown(finalDate, function(event) {
            if(event.strftime('%-D') == 0){
                $this.html(event.strftime('%H:%M:%S'));
            }else{
                $this.html(event.strftime('%-D วัน  %H:%M:%S'));
            }
        });
    });
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
