@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="">
    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7">
                    <div class="single_courses">
                        <h3>{{ $tour->title }}</h3>
                        <div class="content">
                            {!! $tour->body !!}
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5">
                    <div class="courses_sidebar">
                        <div class="video_thumb">
                            <img src="{{ Voyager::image($tour->thumbnail('medium')) }}" alt="">
                            <!-- <a class="popup-video" href="https://www.youtube.com/watch?v=AjgD3CvWzS0">
                                <i class="fa fa-play"></i>
                            </a> -->
                        </div>
                        <div class="author_info">
                            <div class="auhor_header">
                                <div class="thumb">
                                    <img src="img/latest_blog/author.png" alt="">
                                </div>
                                <div class="name">
                                    @if(isset($tour->discount) && (NOW() >= $tour->promotion_start) && (NOW() <=
                                            $tour->promotion_end))
                                        <h3>ราคา : {{ $tour->discount ? number_format($tour->discount) . 'บาท' : 'ยังไม่กำหนดราคา' }}</h3>
                                        @if($tour->price)
                                        <p style="text-decoration: line-through;">ราคาเดิม : {{ number_format($tour->price) }} บาท</p>
                                        @endif
                                    @else
                                        <h3>ราคา : {{ $tour->price ? number_format($tour->price) . 'บาท' : 'ยังไม่กำหนดราคา' }}</h3>
                                    @endif
                                </div>
                            </div>
                            <p class="text_info">
                                <i class="fa fa-clock-o"></i> {{ $tour->created_at }}
                            </p>
                        </div>
                        <div class="row">
                            <div class="col-5">
                                <div class="text-center my-3">
                                    <div class="number">
                                        <span class="minus">-</span>
                                        <input type="text" class="numeric" value="1" disabled style="cursor: no-drop;"/>
                                        <span class="plus">+</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-7">
                                @guest
                                <a href="{{ route('login') }}" class="boxed_btn">Add to cart</a>
                                @else
                                <a href="{{ route('insurance.payment',$tour->slug) }}" class="boxed_btn">Add to cart</a>
                                @endguest
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@push('custom-scripts')
<script>
    $(document).ready(function () {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 1 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
</script>

@endpush
@stop
@section('footer')
@include('frontend.layouts.footer')
@stop
