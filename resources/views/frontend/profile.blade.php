@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')

@push('custom-scripts')
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/css/jasny-bootstrap.min.css">
<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/4.0.0/js/jasny-bootstrap.min.js"></script>

<link rel="stylesheet" href="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.css') }}">
@endpush
<main class="">
    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-5 text-center">
                    <h2>ข้อมูลส่วนตัว</h2>
                </div>
                <div class="col-xl-4 col-lg-4">
                    <div class="courses_sidebar">
                        <div class="author_info">
                            <div class="auhor_header">
                                <div class="thumb">
                                    <img src="" alt="">
                                </div>
                                <div class="name">
                                    <h3>Welcom, {{ Auth::user()->name }}</h3>
                                    <p class="text_info">ยินดีด้วย! คุณคือสมาชิกทั่วไปของเราแล้ว</p>
                                    @guest
                                    @else
                                    @if(Auth::user()->affiliate_id)
                                    {{-- <a href="{{url('/').'/?ref='.Auth::user()->affiliate_id}}">
                                    {{Auth::user()->affiliate_id}}
                                    </a> --}}
                                    <!-- Target -->
                                    <div class="input-group mb-3">
                                        <input id="clipboard" class="form-control"
                                            value="{{url('/').'/?ref='.Auth::user()->affiliate_id}}"
                                            aria-label="Recipient's username" aria-describedby="basic-addon2">
                                        <div class="input-group-append">
                                            <button data-clipboard-target="#clipboard"
                                                class="btn-clipboard btn btn-outline-secondary"
                                                type="button">Coppy</button>
                                        </div>
                                    </div>
                                    @endif
                                    @endguest
                                </div>
                            </div>
                            <a href="javascript:;" data-clipboard-target="#clipboard" class="btn-clipboard genric-btn default circle wfull mb-3"><b>แนะนำเพื่อน</b></a>
                            <hr>
                            <p>
                                @if (Auth::user()->file_id_card)
                                <a href="{{ route('profile.business') }}" id="regis_business"
                                    class="genric-btn primary circle wfull mb-3"><b>แก้ไข</b></a>
                                @else
                                <a href="javascript:;" id="business_btn" class="genric-btn primary circle wfull mb-3">
                                <!-- <a href="{{ route('profile.business') }}" id="regis_business" class="genric-btn primary circle wfull mb-3"> -->
                                    <b>สมัครสมาชิกเป็นธุรกิจ</b>
                                </a>
                                @endif
                            </p>
                        </div>
                    </div>
                    <a href="" class="genric-btn danger-border circle wfull mb-3">ออกจากระบบ</a>
                </div>
                <div class="col-xl-8 col-lg-8 author_info">
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                    <form class="form-contact" action="{{ route('profile.update', Auth::user()->id) }}" method="post" id="FormProfile">
                    @method('PUT')
                    <!-- CSRF TOKEN -->
                    @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="name" id="name" class="form-control" value="{{ Auth::user()->name }}"
                                        placeholder="ชื่อ" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="last_name" id="last_name" class="form-control" value="{{ Auth::user()->last_name }}"
                                        placeholder="นามสกุล" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="email" name="email" id="email" class="form-control" value="{{Auth::user()->email }}"
                                        placeholder="อีเมล" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <input type="text" name="phone" id="phone" class="form-control int" maxlength="10" value="{{ Auth::user()->phone }}"
                                        placeholder="เบอร์โทร" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input name="sub_district" class="form-control" type="text" value="{{ Auth::user()->sub_district }}" placeholder="ตำบล" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input name="district" class="form-control" type="text" value="{{ Auth::user()->district }}" placeholder="อำเภอ" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input name="province" class="form-control" type="text" value="{{ Auth::user()->province }}"
                                        placeholder="จังหวัด" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input name="postcode" class="form-control int" maxlength="5" type="text" value="{{ Auth::user()->postcode }}"
                                        placeholder="รหัสไปรษณีย์" required>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <button type="submit" class="genric-btn info circle mb-3">บันทึกข้อมูลส่วนตัว</button>
                        </div>
                    </form>

                    <div id="business" style="display: none;">
                    <hr>
                    <div class="mb-3">
                        <h2>สมัครสมาชิกเป็นธุรกิจ</h2>
                        <span>** กรุณากรอกข้อมูลให้ครบถ้วน</span>
                    </div>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div><br />
                    @endif
                    <form class="form-contact" action="{{ route('business.update', Auth::user()->id) }}" method="post" enctype="multipart/form-data" id="FormProfile">
                    @method('PUT')
                    <!-- CSRF TOKEN -->
                    @csrf
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="bank" class="form-control" value="{{ Auth::user()->bank }}"
                                        placeholder="ชื่อธนาคาร" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="book_bank_name" class="form-control" value="{{ Auth::user()->book_bank_name }}"
                                        placeholder="ชื่อบัญชี" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="book_bank_number" class="form-control int" maxlength="15" value="{{ Auth::user()->book_bank_number }}"
                                        placeholder="เลขที่บัญชี" required>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <input type="text" name="id_card" class="form-control int" maxlength="13" value="{{ Auth::user()->id_card }}"
                                        placeholder="รหัสประจำตัวประชาชน" required>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    @if (Auth::user()->file_id_card)
                                    <a href="{{ url('storage/'.Auth::user()->file_id_card) }}" target="_blank">
                                        <p class="mb-2" style="color: rgb(0, 123, 255);">คลิกดูสำเนาประจำตัวประชาชน <i class="fa fa-external-link"></i></p>
                                    </a>
                                    @endif
                                    <!-- <div class="custom-file">
                                        <input type="file" class="custom-file-input" name="file_id_card">
                                        <label class="custom-file-label" for="">เลือกไฟล์ของคุณ</label>
                                    </div> -->
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput" style="height: 40px;">
                                            <span class="fileinput-filename">-- เลือกไฟล์สำเนาประจำตัวประชาชน --</span>
                                        </div>
                                        <span class="input-group-append">
                                            <span class="input-group-text fileinput-exists" data-dismiss="fileinput">ลบ</span>
                                            <span class="input-group-text btn-file">
                                                <span class="fileinput-new">อัปโหลด</span>
                                                <span class="fileinput-exists">เปลี่ยน</span>
                                                <input type="file" name="file_id_card" accept="*">
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="form-group">
                                    @if (Auth::user()->file_book_bank)
                                    <a href="{{ url('storage/'.Auth::user()->file_book_bank) }}" target="_blank">
                                        <p class="mb-2" style="color: rgb(0, 123, 255);">คลิกดูสำเนาบัณชีธนาคาร <i class="fa fa-external-link"></i></p>
                                    </a>
                                    @endif
                                    <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                        <div class="form-control" data-trigger="fileinput" style="height: 40px;">
                                            <span class="fileinput-filename">-- เลือกไฟล์สำเนาบัณชีธนาคาร --</span>
                                        </div>
                                        <span class="input-group-append">
                                            <span class="input-group-text fileinput-exists" data-dismiss="fileinput">ลบ</span>
                                            <span class="input-group-text btn-file">
                                                <span class="fileinput-new">อัปโหลด</span>
                                                <span class="fileinput-exists">เปลี่ยน</span>
                                                <input type="file" name="file_book_bank" accept="*">
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group mt-3">
                            <a href="{{ route('business.payment') }}">
                                <button type="submit" class="genric-btn info circle mb-3">บันทึกสมัครสมาชิก</button>
                            </a>
                        </div>
                    </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
</main>
@stop
@push('custom-scripts')
<script>
$("#business_btn").click(function () {
    $("#business").css("display", "block");
    $('html,body').animate({scrollTop: $($("#business")).offset().top-100}, 800);
});
</script>
<script>
$(".int").on("keypress keyup blur",function (event) {    
    $(this).val($(this).val().replace(/[^\d].+/, ""));
    if ((event.which < 48 || event.which > 57)) {
        event.preventDefault();
    }
});
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script>
    var clipboard = new ClipboardJS('.btn-clipboard');

    clipboard.on('success', function(e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);
        alert("Coppied!")
        e.clearSelection();
    });
    clipboard.on('error', function(e) {
    console.error('Action:', e.action);
    console.error('Trigger:', e.trigger);
});
</script>

<!-- dependencies for zip mode -->
<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/zip.js/zip.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/JQL.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('plugin_front/jquery.Thailand/dependencies/typeahead.bundle.js') }}"></script>

<script src="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.js') }}"></script>
<script type="text/javascript">
    $.Thailand({
        database: "{{ asset('plugin_front/jquery.Thailand/db.json') }}",

        $district: $('[name="sub_district"]'),
        $amphoe: $('[name="district"]'),
        $province: $('[name="province"]'),
        $zipcode: $('[name="postcode"]'),

        onDataFill: function (data) {
            console.info('Data Filled', data);
        },

        onLoad: function () {
            console.info('Autocomplete is ready!');
            $('#loader, .demo').toggle();
        }
    });

    // watch on change

    $('[name="sub_district"]').change(function () {
        console.log('ตำบล', this.value);
    });
    $('[name="district"]').change(function () {
        console.log('อำเภอ', this.value);
    });
    $('[name="province"]').change(function () {
        console.log('จังหวัด', this.value);
    });
    $('[name="postcode"]').change(function () {
        console.log('รหัสไปรษณีย์', this.value);
    });
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
