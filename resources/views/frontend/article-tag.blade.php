@extends('frontend.main')
@section('title', setting('site.title'))
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
@isset($banners)
{{-- @include('frontend.slide.banner-video')  --}}
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="mt-5">
    <div class="container">
        @if(sizeof($tags) > 0)
        <section class="pt-5">
            <h1 class="h3 text-left mb-3">Tag : {{ $tag }}</h1>
            <hr>
            @foreach ($tags as $post)
            <!--Grid row-->
            <div class="row wow fadeIn">
                <!--Grid column-->
                <div class="col-lg-5 col-xl-4 mb-4">
                    <!--Featured image-->
                    <div class="view overlay rounded z-depth-1">
                        <img src="{{ Voyager::image($post->image) }}" class="img-fluid" alt="{{ $post->title }}">
                        <a href="{{ route('articles.show',$post->slug) }}" target="_self">
                            <div class="mask rgba-white-slight"></div>
                        </a>
                    </div>
                </div>
                <!--Grid column-->
                <!--Grid column-->
                <div class="col-lg-7 col-xl-7 ml-xl-4 mb-4">
                    <a href="{{ route('articles.show',$post->slug) }}" target="_self">
                        <h3 class="mb-3 font-weight-bold dark-grey-text">
                            <strong>{{ $post->title }}</strong>
                        </h3>
                    </a>
                    <p class="grey-text">{{ $post->excerpt }}</p>
                    <a href="{{ route('articles.show',$post->slug) }}" target="_self"
                        class="btn btn-primary btn-md">อ่านเพิ่มเติม
                        <i class="fas fa-play ml-2"></i>
                    </a>
                </div>
                <!--Grid column-->
            </div>
            <!--Grid row-->
            <hr class="mb-5">
            @endforeach
        </section>
        <!--Section: More-->
        @endisset
    </div>
</main>
@stop
@section('footer')
@include('frontend.layouts.footer')
@stop
