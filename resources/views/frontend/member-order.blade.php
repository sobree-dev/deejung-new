@extends('frontend.main')
@section('title', isset($title) ? $title : setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')

@push('custom-scripts')
<link rel="stylesheet" href="{{ asset('plugin_front/jquery.Thailand/jquery.Thailand.min.css') }}">
@endpush
<main class="">
    <div class="courses_details_info">
        <div class="container">
            <div class="row">
                <div class="col-12 mb-3 text-center">
                    <h2>ประวัติการสั่งซื้อ</h2>
                </div>
                <div class="col-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Order / วันที่สั่งซื้อ</th>
                                <th>ชื่อรายการ</th>
                                <th>ราคา x จำนวน</th>
                                <th>ราคารวม</th>
                                <th class="text-right">สถานะ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($orders as $key => $order)
                            <tr>
                                <td>
                                    <b class="c-o">{{ $order->code }}</b><br>
                                    <span>{{ DateThai($order->created_at) }}</span>
                                </td>
                                <td>
                                    @foreach($order->details as $detail)
                                    {{ $detail->tour->title }} <br>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($order->details as $detail)
                                    {{ number_format($detail->price) }} X {{ number_format($detail->amount) }} <br>
                                    @endforeach
                                </td>
                                <td>{{ number_format($order->price_summary, 2) }} บาท</td>
                                <td class="text-right">
                                    <span class="badge {{ stateColor($order->state) }}">{{ $order->state }}</span>
                                    <a href="{{ route('member.order.detail', $order->id) }}">
                                        <p>ดูรายละเอียด</p>
                                    </a>
                                </td>
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center">คุณยังไม่มีรายการสั่งซื้อ</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                </div>
                @if($orders->hasPages())
                <div class="col-12 d-flex justify-content-end">
                    {{ $orders->links() }}
                </div>
                @endif
            </div>
        </div>
    </div>
</main>

@stop
@push('custom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>
<script>
    var clipboard = new ClipboardJS('.btn-clipboard');

    clipboard.on('success', function (e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);
        alert("Coppied!")
        e.clearSelection();
    });
    clipboard.on('error', function (e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });

</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
