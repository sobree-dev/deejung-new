@extends('frontend.main')
@section('title', $article->title ?? setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@if(setting('web-seo.sharethis'))
{!! setting('web-seo.sharethis') !!}
@endif
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
{{-- @include('frontend.slide.banner-image') --}}
@endisset
@stop
@section('content')
<main class="mt-5" style="margin-top: 10%!important">
    <div class="container">
        <section class="content">
            <h1 class="h3 text-left mb-3">เกี่ยวกับเรา</h1>
            @if($page->image)
            <div class="row">
                <div class="col-md-12 mb-5 mt-3">
                    <img src="{{ Voyager::image( $page->image) }}" class="img-fluid" alt="Responsive image">
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-md-12 mb-5 mt-3">
                    {!! $page->body !!}
                </div>
            </div>
        </section>
    </div>
</main>
@stop
@push('custom-scripts')
<script>
    $(".fancybox").fancybox();
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
