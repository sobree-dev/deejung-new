@extends('frontend.main')
@section('title', $article->title ?? setting('site.title'))
@section('seo')
@include('frontend.layouts.seo')
@if(setting('web-seo.sharethis'))
{!! setting('web-seo.sharethis') !!}
@endif
@stop
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
{{-- @include('frontend.slide.banner-video')  --}}
@isset($banners)
{{-- @include('frontend.slide.banner-image') --}}
@endisset
@stop
@section('content')
<main class="pt-5 {{ strtolower($page->title) }}" style="margin-top: 5%!important">
    <div class="container ">
        <section class="content">
            <h1 class="h3 text-left mb-3">ติดต่อเรา</h1>
            <div class="row">
                <div class="col-md-6">
                    <p class="mb-5">
                        {!! $page->body ?? null !!}
                    </p>
                    @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    @if(Session::has('alert-type'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{{ Session::get('message') }}</li>
                        </ul>
                    </div>
                    @endif
                    {{-- form  --}}
                    <form method="post" action="{{ route('contact-us.store') }}" id="contact">
                        @csrf
                        <div class="form-group row">
                            <div class="col-md-6 mb-4">
                                <input type="text" class="form-control" placeholder="ชื่อ" name="first_name"
                                    value="{{ old('first_name') }}" required>
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="นามสกุล" name="last_name"
                                    value="{{ old('last_name') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="text" class="form-control phone" placeholder="เบอร์โทรศัพท์" name="phone"
                                    value="{{ old('phone') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="email" class="form-control" placeholder="Email" name="email"
                                    value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <textarea class="form-control" id="" cols="30" rows="10" placeholder="ข้อความ"
                                    name="messenge" required>{{ old('messenge') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <input type="submit" class="btn btn-primary py-3 px-5 btn-block" value="ส่งข้อความ">
                            </div>
                        </div>
                    </form>
                </div>
                @if($page->image)
                <div class="col-md-6">
                    <img class="img-fluid" src="{{ Voyager::image( $page->image) }}" alt="">
                </div>
                @endif
            </div>
        </section>
    </div>
</main>
@stop
@push('custom-scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/additional-methods.min.js"></script>
<script src="{{ asset('plugin/input-mask/jquery.mask.min.js')}}"></script>
<script>
    $( "#contact" ).validate({
  rules: {
    field: {
      required: true,
      email: true
    }
  },
  messages: {
    first_name: {
	    required: 'กรุณาระบุชื่อ'
	},
    last_name: {
	    required: 'กรุณาระบุนามสกุล'
	},
    phone: {
	    required: 'กรุณาระบุเบอร์โทรศัพท์'
	},
    email: {
	    required: 'กรุณาระบุชื่ออีมเล',
        email: 'กรุณาอีเมลที่ถูกต้อง'
	},
    messenge: {
	    required: 'กรุณาระบุข้อความ'
	}
  }
});

//phone
$(".phone").mask("Z00 000-0000", {
    translation: {
        Z: { pattern: /[0]/ }
    }
});
</script>
@endpush
@section('footer')
@include('frontend.layouts.footer')
@stop
