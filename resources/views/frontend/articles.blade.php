@extends('frontend.main')
@section('title', setting('site.title'))
@section('navbar')
@include('frontend.layouts.navbar',['logo' => 'logo.png'])
@stop
@section('slides')
@isset($banners)
{{-- @include('frontend.slide.banner-video')  --}}
@include('frontend.slide.banner-image')
@endisset
@stop
@section('content')
<main class="mt-5">
    <div class="container">
        <h1 class="h3 text-left mb-3">บทความ</h1>
        <div class="row mb-4 ml-2">
            <a class="btn btn-primary {{ is_null($category) || $category=='all' ? 'active' : '' }}"
                href="{{ route('articles.index').'?category=all' }}">ทั้งหมด</a>
            @foreach ($categories as $cat)
            <a class="btn btn-primary {{ ($category == $cat->slug) ? 'active' : '' }}"
                href="{{ route('articles.index').'?category='.$cat->slug }}">{{ $cat->name }}</a>
            @endforeach
            <div class="float-right">
            </div>
        </div>
        @if(sizeof($articles) > 0)
        <div class="row">
            @foreach ($articles as $article)
            <!-- Grid column -->
            <div class="col-lg-4 mb-5">
                <a class="d-block rounded-lg lift lift-lg" href="{{ route('articles.show',$article->slug) }}">
                    <img src="{{ Voyager::image($article->thumbnail('medium')) }}" class="img-fluid rounded-lg"
                        alt="{{ $article->title }}">
                </a>
                <a href="{{ route('articles.show',$article->slug) }}">
                    <h1 class="h3 text-left mt-4">{{ $article->title}}</h1>
                </a>
                <p class="category">หมวดหมู่ : {{ $article->category->name }}</p>
                <small class="post_date">โฟสเมื่อ : {{ DateThai($article->created_at)}}</small>
                <small class="read">อ่าน : {{ number_format($article->viewer,0)}}</small>
                <p class="grey-text">{{ Str::limit($article->excerpt,120) }}</p>
            </div>
            <!-- Grid column -->
            @endforeach
        </div>
        @else
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="alert alert-danger" role="alert">
                    ไม่พบข้อมูล
                </div>
            </div>
        </div>
        @endif
    </div>
</main>
@stop
@section('footer')
@include('frontend.layouts.footer')
@stop
