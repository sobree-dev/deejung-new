

<!-- header-start -->
<header>
    <div class="header-area ">
        <div id="sticky-header" class="main-header-area">
            <div class="container-fluid p-0">
                <div class="row align-items-center no-gutters">
                    <div class="col-xl-2 col-lg-2">
                        <div class="logo-img">
                            <a href="{{ url('/') }}">
                                <img src="{{ setting('site.logo') ? Voyager::image(setting('site.logo')) : asset('img/logo.png') }}" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-10 col-lg-10">
                        <div class="main-menu  d-none d-lg-block">
                            <nav>
                                {{ menu('web','menu.my_menu') }}
                            </nav>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="mobile_menu d-block d-lg-none"> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- header-end -->
