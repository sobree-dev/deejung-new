<?php

Route::group( ['prefix' => 'admin'], function () {
    Voyager::routes();

    //roles update
    Route::match( ['put', 'patch'], 'my-roles/{role}', 'Voyager\VoyagerRoleController@update' )->name( 'voyager.roles.my-update' );
    //setting
    Route::get( '/websetting', 'Voyager\VoyagerSettingsController@websetting' )->name( 'websetting.index' );

    //summernote
    Route::post( 'summernote', 'Formfield\SummernoteController@upload' );

    //slug
    Route::post( '/check/check-dup-title/', ['uses' => 'CheckduptitleController@checkDupTitle'] )->name( 'check.check-dup-title' );

    Route::post( '/check/check-dup-slug/', ['uses' => 'CheckduptitleController@checkDupSlug'] )->name( 'check.check-dup-slug' );

    //memeber
    Route::get( '/members', 'MemberController@index' )->name( 'voyager.member.index' );
    Route::get( '/members/{id}/edit', 'MemberController@edit' )->name( 'voyager.member.edit' );
    Route::get( '/members/{id}', 'Voyager\VoyagerUserController@show' )->name( 'voyager.member.show' );
    Route::delete( '/members/{$id}', 'MemberController@destroy' )->name( 'voyager.member.destroy' );
    Route::put( '/members/{id}', 'Voyager\VoyagerUserController@update' )->name( 'voyager.member.update' );

    //group member
    Route::get( '/business-group', 'MemberController@businessGroup' )->name( 'voyager.business-group.index' );
    Route::get( '/business-group/{id}', 'MemberController@businessGroupShow' )->name( 'voyager.business-group.show' );
    //log Reader : admin/log-reader
    Route::group(
        [
            'namespace'  => '\Haruncpi\LaravelLogReader\Controllers',
            'middleware' => ['auth'],
        ],
        function () {
            Route::get( config( 'laravel-log-reader.view_route_path' ), 'LogReaderController@getIndex' );
            Route::post( config( 'laravel-log-reader.view_route_path' ), 'LogReaderController@postDelete' );
            Route::get( config( 'laravel-log-reader.api_route_path' ), 'LogReaderController@getLogs' );
        }
    );

    // order
    Route::match( ['post'], '/orders/{status}/{id}', [
        'as'   => 'order.changeStatus',
        'uses' => 'OrderController@changeOrder',
    ] );

    // transection
    Route::match( ['post'], '/transection/withdraw/{status}/{id}', [
        'as'   => 'transection.changeStatus',
        'uses' => 'TransectionController@changeStatus',
    ] );

} );
