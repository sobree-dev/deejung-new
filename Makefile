up: start
down: stop
up.www: start.www


start:
	@echo "Run all services"
	cd ./laradock && docker-compose up -d nginx mysql phpmyadmin workspace

start.www:
	@echo "Compiler asset"
	npm run watch

stop:
	cd ./laradock && docker-compose down
