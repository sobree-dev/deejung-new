<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderCode extends Model
{
    protected $fillable = ['tour_id', 'order_code'];
}
