<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Resizable;
use Illuminate\Database\Eloquent\Builder;

class Customer extends Model
{
    use SoftDeletes,
        Resizable;

    /**
     * Log
     */
    protected static $logName = 'Customer';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "This model Customer to {$eventName}";
    }

    public function scopeOrder(Builder $query)
    {
        return $query->orderBy('order', 'asc')->orderBy('updated_at', 'desc');
    }
}
