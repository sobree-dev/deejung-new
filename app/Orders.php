<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Orders extends Model
{
    //
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['tour_id', 'user_id', 'agent_id', 'code', 'amount', 'price', 'price_summary', 'state', 'payment_type', 'slip'];

    public function user()
    {
        return $this->belongsTo( 'App\User' );
    }

    public function agent()
    {
        return $this->belongsTo( 'App\User' );
    }

    public function details()
    {
        return $this->hasMany( 'App\OrderDetail', 'order_id', 'id' );
    }

    public function insurance()
    {
        return $this->hasOne( 'App\Insurance', 'order_id', 'id' );
    }
}
