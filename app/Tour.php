<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;

class Tour extends Model
{
    use SoftDeletes, Translatable, Resizable;

    protected $translatable = ['title', 'seo_title', 'excerpt', 'body', 'slug', 'meta_description', 'meta_keywords'];

    protected $dates = ['deleted_at'];

    const PUBLISHED = 'PUBLISHED';
    const FEATURED = 1;

    /**
     * Log
     */
    protected static $logName = 'Tour';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "This model Tour to {$eventName}";
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

   /* public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && Auth::user()) {
            $this->author_id = Auth::user()->getKey();
        }

        parent::save();
    }*/

    public function authorId()
    {
        return $this->belongsTo(Voyager::modelClass('User'), 'author_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Voyager::modelClass('Category'));
    }

    public function scopePublished(Builder $query)
    {
        return $query->where('status', '=', static::PUBLISHED);
    }

    public function scopeOrder(Builder $query)
    {
        return $query->orderBy('order', 'asc')->orderBy('updated_at', 'desc');
    }

    public function categorytour()
    {
        return $this->hasOne(Category::class,'id','category_id');
    }
}
