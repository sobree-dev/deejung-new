<?php
function hex2rgba( $color, $opacity = false )
{

    $default = 'rgb(0,0,0)';

    //Return default if no color provided
    if ( empty( $color ) ) {
        return $default;
    }

    //Sanitize $color if "#" is provided
    if ( $color[0] == '#' ) {
        $color = substr( $color, 1 );
    } else {
        $color = stringToColorCode( $color );
        if ( $color[0] == '#' ) {
            $color = substr( $color, 1 );
        }
    }

    //Check if color has 6 or 3 characters and get values
    if ( strlen( $color ) == 6 ) {
        $hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
    } elseif ( strlen( $color ) == 3 ) {
        $hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
    } else {
        return $default;
    }

    //Convert hexadec to rgb
    $rgb = array_map( 'hexdec', $hex );

    //Check if opacity is set(rgba or rgb)
    if ( $opacity ) {
        if ( abs( $opacity ) > 1 ) {
            $opacity = 1.0;
        }

        $output = 'rgba(' . implode( ",", $rgb ) . ',' . $opacity . ')';
    } else {
        $output = 'rgb(' . implode( ",", $rgb ) . ')';
    }

    //Return rgb(a) color string

    return $output;
}

function stringToColorCode( $str )
{
    $code = dechex( crc32( $str ) );
    $code = substr( $code, 0, 6 );

    return $code;
}

function DateThai( $strDate, $showTime = true )
{
    $strYear      = date( "Y", strtotime( $strDate ) ) + 543;
    $strMonth     = date( "n", strtotime( $strDate ) );
    $strDay       = date( "j", strtotime( $strDate ) );
    $strHour      = date( "H", strtotime( $strDate ) );
    $strMinute    = date( "i", strtotime( $strDate ) );
    $strSeconds   = date( "s", strtotime( $strDate ) );
    $strMonthCut  = array( "", "ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค." );
    $strMonthThai = $strMonthCut[$strMonth];

    $day = "$strDay $strMonthThai $strYear";

    return $showTime ? "$day, $strHour:$strMinute น." : "$day";
}

function orderCode( $id, $latestOrder )
{

    $code = '#' . date( 'y' ) . $id . '-' . str_pad( $latestOrder, 6, "0", STR_PAD_LEFT );

    return $code;
}

function stateColor( $state )
{
    $stateClass = [
        'PENDING'    => 'bg-warning',
        'CHECKING'   => 'bg-info',
        'CONFIRMMED' => 'bg-success',
        'REJECTED'   => 'bg-danger',
        'CANCEL'     => 'bg-default',
    ];

    return $stateClass[$state];
}
