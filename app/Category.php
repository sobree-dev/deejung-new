<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Traits\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

class Category extends Model
{
    use Translatable, SoftDeletes, LogsActivity;

    protected $dates = ['deleted_at'];

    protected $translatable = ['slug', 'name'];

    protected $table = 'categories';

    protected $fillable = ['slug', 'name'];

    /**
     * Log
     */
    protected static $logName = 'Category';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "This model Category to {$eventName}";
    }

    public function posts()
    {
        return $this->hasMany(Voyager::modelClass('Tour'))
            ->published()
            ->orderBy('created_at', 'DESC');
    }


    public function parentId()
    {
        return $this->belongsTo(self::class);
    }
}
