<?php

namespace App\Http\Controllers\Frontend;

use App\Banner;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Frontend\OrdersController as Paypal;
use App\Http\Requests\Insurance as InsuranceRequest;
use App\Insurance;
use App\OrderDetail;
use App\Orders;
use App\Tour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Str;

class InsuranceController extends Controller
{
    public function index( Request $request )
    {
        $banners = Banner::page( 'tours' )->get();

        $topic = 'แพ็กเก็จประกันภัย ทั้งหมด';
        $url = 'visa.show';
        $tours = Tour::published()
            ->whereIn( 'category_id', [2] )
            ->order()
            ->get();

        return view( 'frontend.insurance', compact( 'tours', 'banners','topic','url' ) );
    }

    public function visa( Request $request )
    {
        $banners = Banner::page( 'tours' )->get();

        $topic = 'แพ็กเก็จ Visa ทั้งหมด';
        $url = 'visa.show';
        $tours = Tour::published()
            ->whereIn( 'category_id', [1] )
            ->order()
            ->get();

        return view( 'frontend.insurance', compact( 'tours', 'banners','topic' ) );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Request $request, $slug )
    {
        //get banner home
        $banners = Banner::where( 'page', 'tours' )->get();

        //service
        $tour = Tour::where( 'slug', $slug )->published()->first();
        //SEO
        $seo = Tour::select( 'seo_title', 'meta_description', 'image' )->published()->where( 'slug', $slug )->first();
        if ( !$tour ) {
            abort( 404 );
        }

        return view( 'frontend.insurance-detail', compact( 'banners', 'tour', 'seo' ) );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( InsuranceRequest $request )
    {
        // validate
        $validated = $request->validated();

        try {
            $order = Orders::create( [
                'user_id'       => auth()->user()->id,
                'agent_id'      => Cookie::get( 'referral' ) ?? null,
                'price_summary' => collect( $validated['orders'] )->sum( function ( $order ) {
                    return $order['price'] ?? 0 * $order['amount'];
                } ),
                'slip'          => $request->hasFile( 'slip' ) ? $request->file( 'slip' )->storeAs( 'slip', Str::random( 40 ) . '.jpg', 'public' ) : null,
                'state'         => $request->payment_type === 'TRANSFER' ? 'CHECKING' : 'PENDING',
                'payment_type'  => $request->payment_type,
            ] );

            $order['code'] = orderCode( $validated['orders'][0]['tour_id'], $order->id );
            $order->save();
            $details = [];

            foreach ( $validated['orders'] as $detail ) {
                $detail = OrderDetail::create( [
                    'order_id'      => $order->id,
                    'tour_id'       => $detail['tour_id'],
                    'amount'        => $detail['amount'],
                    'price'         => $detail['price'] ?? 0,
                    'price_summary' => $detail['price'] ?? 0 * $detail['amount'],
                ] );

                array_push( $details, $detail );
            }

            Insurance::create( [
                'order_id'       => $order->id,
                'insurance'      => $request->insurance,
                'insurance_type' => $request->insurance_type,
                'origin'         => $request->origin,
                'destination'    => $request->destination,
                'date_start'     => $request->date_start,
                'date_end'       => $request->date_end,
                'name'           => $request->name,
                'surname'        => $request->surname,
                'phone'          => $request->phone,
                'card_id'        => $request->card_id,
                'address'        => $request->address,
            ] );

            if ( $order->price_summary == 0 ) {
                $order->state = 'CONFIRMMED';
                $order->save();

                return redirect()->route( 'tours.thank' );

            }

            if ( $request->payment_type === 'PAYPAL' ) {

                try {
                    $paypal = new Paypal();

                    return $paypal->generatePaypal( $order, $details );

                } catch ( \Exception $e ) {
                    return redirect()->route( 'tours.fail' )->withError( ['code' => 'danger', 'message' => "Error processing PayPal payment for Order $order->code!"] );
                }

            }

        } catch ( ModelNotFoundException $exception ) {

            return redirect()->route( 'tours.fail' )->withError( $exception->getMessage() );

        }

        return redirect()->route( 'tours.thank' );

    }

    public function payment( Request $request, $slug )
    {
        //get banner home
        $banners = Banner::where( 'page', 'tours' )->get();

        //service
        $tour = Tour::where( 'slug', $slug )->published()->first();
        //SEO
        $seo = Tour::select( 'seo_title', 'meta_description', 'image' )->published()->where( 'slug', $slug )->first();

        if ( !$tour ) {
            abort( 404 );
        }

        return view( 'frontend.insurance-payment', compact( 'banners', 'tour', 'seo' ) );
    }
}
