<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;
use App\Page;

class AboutusController extends Controller
{
    public function index()
    {
        //get banner home
        $banners = Banner::page('about-us')->get();

        $page  = Page::where('slug', 'about-us')->active()->first();
        if (!$page) {
            abort(404);
        }
        return view('frontend.about-us', compact('page'));
    }
}
