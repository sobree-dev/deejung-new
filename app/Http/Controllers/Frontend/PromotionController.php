<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Promotion;
use App\Banner;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get banner home
        $banners = Banner::page('promotion')->get();

        $promotions = Promotion::published()
            ->order()
            ->get();

        return view('frontend.promotions', compact('promotions', 'banners'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $slug)
    {
        //get banner home
        $banners = Banner::where('page', 'service')->get();

        //service
        $service = Service::where('slug', $slug)->published()->first();
        //SEO
        $seo = Service::select('seo_title', 'meta_description', 'image')->published()->where('slug', $slug)->first();
        if (!$service) {
            abort(404);
        }
        return view('frontend.service-detail', compact('banners', 'service', 'seo'));
    }
}
