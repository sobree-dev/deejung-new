<?php

namespace App\Http\Controllers\Frontend;

use App\Banner;
use App\Http\Controllers\Controller;
use App\Tour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ToursController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get banner home
        $banners = Banner::page( 'tours' )->get();

        $topic = 'แพ็กเก็จทัวร์ทั้งหมด';
        $url = 'tours.show';
        $tours = Tour::published()
            ->whereNotIn( 'category_id', [1, 2] )
            ->order()
            ->get();

        return view( 'frontend.tours', compact( 'tours', 'banners','topic','url') );
    }

    public function insurance()
    {
        //get banner home
        $banners = Banner::page( 'tours' )->get();

        $topic = 'แพ็กเก็จประกัน ทั้งหมด';
        $url = 'insurance.show';
        $tours = Tour::published()
            ->where( 'category_id', 2 )
            ->order()
            ->get();

        return view( 'frontend.tours', compact( 'tours', 'banners','topic','url' ) );
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Request $request, $slug )
    {
        //get banner home
        $banners = Banner::where( 'page', 'tours' )->get();

        //service
        $tour = Tour::where( 'slug', $slug )->published()->first();
        //SEO
        $seo = Tour::select( 'seo_title', 'meta_description', 'image' )->published()->where( 'slug', $slug )->first();
        if ( !$tour ) {
            abort( 404 );
        }

        return view( 'frontend.tours-detail', compact( 'banners', 'tour', 'seo' ) );
    }

    public function payment( Request $request, $slug )
    {
        //get banner home
        $banners = Banner::where( 'page', 'tours' )->get();

        //service
        $tour = Tour::where( 'slug', $slug )->published()->first();
        //SEO
        $seo = Tour::select( 'seo_title', 'meta_description', 'image' )->published()->where( 'slug', $slug )->first();

        if ( !$tour ) {
            abort( 404 );
        }

        return view( 'frontend.tours-payment', compact( 'banners', 'tour', 'seo' ) );
    }

    public function payment_save( Request $request, $id )
    {
        $orders            = null;
        $orders['user_id'] = $request->user_id;
        $orders['tour_id'] = $request->id;
        $orders['price']   = $request->price;

        DB::table( 'orders' )->insert( $orders );

        return redirect()->route( 'home' );
    }
}
