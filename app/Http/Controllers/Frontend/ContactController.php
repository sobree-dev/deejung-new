<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Contact;
use App\Banner;
use App\Page;

class ContactController extends Controller
{
    public function index(Request $request)
    {
        //get banner home
        $banners = Banner::page('contact')->get();

        $page  = Page::where('slug', 'contact-us')->active()->first();
        if (!$page) {
            abort(404);
        }
        return view('frontend.contact-us', compact('banners', 'page'));
    }

    public function store(Request $request)
    {
        $role = [
            'first_name' => 'required|max:255',
            'last_name'  => 'required|max:255',
            'phone'      => 'required',
            'email'      => 'required|email',
            'messenge'   => 'required',
        ];

        $messages = [
            'first_name.required' => 'กรุณาระบุชื่อ',
            'last_name.required'  => 'กรุณาระบุนามสกุล',
            'phone.required'      => 'กรุณาระบุเบอร์โทรศัพท์',
            'email.required'      => 'กรุณาระบุอีเมล',
            'messenge.required'   => 'กรุณาระบุข้อความ',
        ];

        $validator = Validator::make($request->all(), $role, $messages);

        if ($validator->fails()) {
            return back()
                ->withErrors($validator)
                ->withInput();
        }

        $contact             = new Contact;
        $contact->first_name = $request->first_name;
        $contact->last_name  = $request->last_name;
        $contact->phone      = $request->phone;
        $contact->email      = $request->email;
        $contact->messenge   = $request->messenge;
        $save = $contact->save();

        if ($save) {
            return back()->with([
                'message'    => 'ส่งข้อมูลเรียบร้อย',
                'alert-type' => 'success',
            ]);
        }
    }
}
