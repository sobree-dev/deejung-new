<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Http\Controllers\TransectionController as Transection;
use App\Http\Requests\StoreOrders;
use App\OrderDetail;
use App\Orders;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use PayPal;

class OrdersController extends Controller
{
    protected $provider;

    public function __construct()
    {
        $this->provider = PayPal::setProvider( 'express_checkout' );
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Orders::with( ['details', 'details.tour'] )
            ->where( 'user_id', auth()->user()->id )
            ->latest()
            ->paginate( 10 );

        return view( 'frontend.member-order', compact( 'orders' ) );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store( StoreOrders $request )
    {
        // validate
        $validated = $request->validated();

        try {
            $order = Orders::create( [
                'user_id'       => auth()->user()->id,
                'agent_id'      => Cookie::get( 'referral' ) ?? null,
                'price_summary' => collect( $validated['orders'] )->sum( function ( $order ) {
                    return $order['price'] * $order['amount'];
                } ),
                'slip'          => $request->hasFile( 'slip' ) ? $request->file( 'slip' )->storeAs( 'slip', Str::random( 40 ) . '.jpg', 'public' ) : null,
                'state'         => $request->payment_type === 'TRANSFER' ? 'CHECKING' : 'PENDING',
                'payment_type'  => $request->payment_type,
            ] );

            $order['code'] = orderCode( $validated['orders'][0]['tour_id'], $order->id );
            $order->save();
            $details = [];

            foreach ( $validated['orders'] as $detail ) {
                $detail = OrderDetail::create( [
                    'order_id'      => $order->id,
                    'tour_id'       => $detail['tour_id'],
                    'amount'        => $detail['amount'],
                    'price'         => $detail['price'],
                    'price_summary' => $detail['price'] * $detail['amount'],
                ] );

                array_push( $details, $detail );
            }

            if ( $request->payment_type === 'PAYPAL' ) {

                try {

                    return $this->generatePaypal( $order, $details );

                } catch ( \Exception $e ) {
                    return redirect()->route( 'tours.fail' )->withError( ['code' => 'danger', 'message' => "Error processing PayPal payment for Order $order->code!"] );
                }

            }
        } catch ( ModelNotFoundException $exception ) {

            return redirect()->route( 'tours.fail' )->withError( $exception->getMessage() );

        }

        return redirect()->route( 'tours.thank' );
    }

    public function repay( Request $request, $id )
    {
        $validator = Validator::make( $request->all(), [
            'payment_type' => ['required', Rule::in( ['TRANSFER', 'PAYPAL'] )],
            'slip'         => 'required_if:payment_type,TRANSFER|nullable|image',
        ] );

        if ( $validator->fails() ) {
            return back()->withErrors( $validator );
        }

        try {
            $order = Orders::with( 'details' )->findOrFail( $id );

            $order->slip         = $request->hasFile( 'slip' ) ? $request->file( 'slip' )->storeAs( 'slip', Str::random( 40 ) . '.jpg', 'public' ) : null;
            $order->payment_type = $request->payment_type;
            $order->state        = $request->payment_type === 'TRANSFER' ? 'CHECKING' : 'PENDING';

            $order->save();

            if ( $request->payment_type === 'PAYPAL' ) {

                try {

                    $cart = $this->paypal( $order, $order->details );

                    $response = $this->provider->setExpressCheckout( $cart );

                    return redirect( $response['paypal_link'] );
                } catch ( \Exception $e ) {
                    return redirect()->route( 'tours.fail' )->withError( ['code' => 'danger', 'message' => "Error processing PayPal payment for Order $order->code!"] );
                }

            }
        } catch ( ModelNotFoundException $exception ) {

            return redirect()->route( 'tours.fail' )->withError( $exception->getMessage() );

        }

        return redirect()->route( 'tours.thank' );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( $id )
    {
        //
        $order = Orders::with( ['details', 'details.tour', 'insurance'] )
            ->where( 'id', $id )
            ->where( 'user_id', auth()->user()->id )
            ->firstOrFail();

        return view( 'frontend.member-order-detail', compact( 'order' ) );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id )
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        //
    }

    public function generatePaypal( $order, $details )
    {
        $cart = $this->paypal( $order, $details );

        $response = $this->provider->setExpressCheckout( $cart );

        return redirect( $response['paypal_link'] );

    }

    public function paypal( $order, $details )
    {
        $data = ['items' => []];

        foreach ( $details as $detail ) {
            $item = OrderDetail::with( 'tour' )
                ->where( 'order_id', $detail->order_id )
                ->where( 'tour_id', $detail->tour_id )
                ->first();

            array_push( $data['items'], [
                'name'  => $item->tour->title,
                'price' => $item->price,
                'qty'   => $item->amount,
            ] );
        }

        $data['subscription_desc'] = config( 'paypal.invoice_prefix' ) . '_' . $order->code;

        $data['return_url'] = route( 'tours.paypal.success' );
        $data['cancel_url'] = route( 'tours.fail' );
        $data['total']      = $order->price_summary;

        $data['invoice_id']          = $order->code;
        $data['invoice_description'] = config( 'paypal.invoice_prefix' ) . '_' . $order->code;

        return $data;

    }

    public function paypalSuccess( Request $request )
    {
        $token   = $request->get( 'token' );
        $PayerID = $request->get( 'PayerID' );

        $response = $this->provider->getExpressCheckoutDetails( $token );

        if ( in_array( strtoupper( $response['ACK'] ), ['SUCCESS', 'SUCCESSWITHWARNING'] ) ) {
            $code = $response['INVNUM'];

            $order = Orders::with( ['details', 'details.tour'] )->where( 'code', $code )->first();

            $cart = $this->paypal( $order, $order->details );

            $paymentStatus = $this->provider->doExpressCheckoutPayment( $cart, $token, $PayerID );

            if ( !strcasecmp( $paymentStatus['PAYMENTINFO_0_PAYMENTSTATUS'], 'Completed' ) || !strcasecmp( $paymentStatus['PAYMENTINFO_0_PAYMENTSTATUS'], 'Processed' ) ) {
                $order->state = 'CONFIRMMED';
                Transection::store( 'TOUR', 'COMMISTION', $order->agent_id, $order->price_summary, $order->id, $order->user_id );
            } else {
                $order->state = "PENDING";
            }

            $order->save();

            return redirect()->route( 'tours.thank' );

        }

        return redirect()->route( 'tours.fail' );

    }
}
