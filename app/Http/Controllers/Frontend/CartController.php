<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tour;
use App\Banner;

class CartController extends Controller
{

    public function index()
    {
        return view('frontend.cart');
    }

    public function addToCart($id)
    {
        $tour = Tour::find($id);
    
        if(!$tour) {
    
            abort(404);
    
        }
    
        $cart = session()->get('cart');
    
        // if cart is empty then this the first product
        if(!$cart) {
    
            $cart = [
                    $id => [
                        "name" => $tour->title,
                        "quantity" => 1,
                        "price" => $tour->price,
                        "photo" => $tour->image
                    ]
            ];
    
            session()->put('cart', $cart);
    
            return redirect()->back()->with('success', 'Product added to cart successfully!');
        }
    
        // if cart not empty then check if this product exist then increment quantity
        if(isset($cart[$id])) {
    
            $cart[$id]['quantity']++;
    
            session()->put('cart', $cart);
    
            return redirect()->back()->with('success', 'Product added to cart successfully!');
    
        }
    
        // if item not exist in cart then add to cart with quantity = 1
        $cart[$id] = [
            "name" => $tour->title,
            "quantity" => 1,
            "price" => $tour->price,
            "photo" => $tour->image
        ];
    
        session()->put('cart', $cart);
    
        return redirect()->back()->with('success', 'Product added to cart successfully!');
    }

}
