<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Tour;
use App\Banner;
use App\User;
use Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
class ProfileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //get banner home
        $banners = Banner::page('tours')->get();

        $tours = Tour::published()
            ->order()
            ->get();

        return view('frontend.profile', compact('tours', 'banners'));
    }

    public function business()
    {
        //get banner home
        $banners = Banner::page('tours')->get();

        $tours = Tour::published()
            ->order()
            ->get();

        return view('frontend.profile-business', compact('tours', 'banners'));
    }

    public function payment()
    {
        //get banner home
        $banners = Banner::page('tours')->get();

        $tours = Tour::published()
            ->order()
            ->get();

        return view('frontend.profile-payment', compact('tours', 'banners'));
    }

    public function update(Request $request, $id)
    {
      
        $rules = [
            'name'            =>  'required',
            'last_name'             =>  'required',
            'email'                 =>  'required|email',
        ];

        $messages = [
            'name.required'   =>  'Your first name is required.',
            'last_name.required'    =>  'Your last name is required.',
            'email.required'        =>  'Your emails address is required.',
            'email.email'          =>  'That email address is format in use.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
        
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{
            $user = User::find($id);
            $user->name = $request->name;
            $user->last_name = $request->last_name;
            $user->email = $request->email;
            $user->phone = $request->phone;
            $user->sub_district = $request->sub_district;
            $user->district = $request->district;
            $user->province = $request->province;
            $user->postcode = $request->postcode;
            if($user->email !== $request->email){
                $user->email = $request->email;
            }
            
            $user->save();

            Session::flash('success', 'Your profile was updated.');
            return redirect()->route('profile');
        }
    }

    public function businessupdate(Request $request, $id)
    {
      
        $rules = [
            'bank'                      =>  'required',
            'book_bank_name'             =>  'required',
            'book_bank_number'            =>  'required',
            'id_card'             =>  'required',
        ];

        $messages = [
            'bank.required'   =>  'Your bank is required.',
            'book_bank_name.required'    =>  'Your book_bank_name is required.',
            'book_bank_number.required'    =>  'Your book_bank_number is required.',
            'id_card.required'        =>  'Your id_card is required.',
        ];

        $validator = Validator::make($request->all(), $rules, $messages);

        if($validator->fails()){
        
            return back()
                ->withErrors($validator)
                ->withInput();
        }else{

            $path1 = null;
            $path2 = null;
            $file1 = $request->file('file_id_card');
            $file2 = $request->file('file_book_bank');
            Log::debug($file1);
            if(!empty($file1)):
                $type = $file1->extension();
                $path1 = $file1->store('business', 'public');
            endif;
            if(!empty($file2)):
                $type = $file2->extension();
                $path2 = $file2->store('business', 'public');
            endif;
           
            $user = User::find($id);

            $file_id_card = $user->file_id_card; 
            $file_book_bank = $user->file_book_bank; 
            $user->bank = $request->bank;
            $user->book_bank_name = $request->book_bank_name;
            $user->book_bank_number = $request->book_bank_number;
            $user->id_card = $request->id_card;
            if(!empty($path1)):
                $user->file_id_card = $path1;
            endif;
            if(!empty($path2)):
                $user->file_book_bank = $path2;
            endif;
           

            $user->save();

            if(!empty($path1)):
                Storage::delete('public/'.$file_id_card);
            endif;
            if(!empty($path2)):
                Storage::delete('public/'.$file_book_bank);
            endif;
            //  @unlink(storage_path('app/public/'.$file_id_card));
            

            Session::flash('success', 'Your profile was updated');
            return redirect()->route('profile.business');
        }
    }

}
