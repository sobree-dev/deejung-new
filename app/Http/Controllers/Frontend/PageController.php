<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function thank()
    {
        return view('frontend.page-thank');
    }

    public function fail()
    {
        return view('frontend.page-fail');
    }


}
