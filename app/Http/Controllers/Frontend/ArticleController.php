<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Banner;
use App\Post;
use App\Category;
use App\TaggableTag;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;


class ArticleController extends Controller
{
    public function index(Request $request)
    {

        //get banner home
        $banners = Banner::page('article')->get();

        //category
        $categories = Category::all();

        //get post
        $articles = Post::select(DB::raw('posts.*,categories.name'))->published()->publicat()
            ->order();

        $articles = $articles->leftJoin('categories', 'posts.category_id', '=', 'categories.id');

        $category = null;
        if ($request->category) {
            $category = $request->category;
            if ($category != 'all') {
                $articles = $articles->where('categories.slug', '=', $category);
            }
        }
        $articles = $articles->get();


        return view('frontend.articles', compact('articles', 'banners', 'categories', 'category'));
    }

    public function show(Request $request, $slug)
    {   
        //get banner home
        $banners = Banner::where('page', 'article')->get();

        //service
        $article = Post::published()->publicat()->where('slug', $slug)->first();

        if (!$article) {
            abort(404);
        }
        $article->category;

        $post = Post::find($article->id);
        $post->viewer = $post->viewer + 1;
        $post->save();

        //SEO
        $seo = Post::select('seo_title', 'meta_description', 'image')->published()->where('slug', $slug)->first();

        return view('frontend.article-detail', compact('banners', 'article', 'seo'));
    }

    public function showTag(Request $request, $tag)
    {
        //get banner home
        $banners = Banner::where('page', 'article')->get();

        $tag = str_replace('-', ' ', $tag);

        $tags = TaggableTag::tagslug($tag)->get();

        return view('frontend.article-tag', compact('banners', 'tags', 'tag'));
    }
}
