<?php

namespace App\Http\Controllers;

use App\Transection;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class TransectionController extends Controller
{

    public static function index( Request $request )
    {
        return Transection::with( ['order.details.tour', 'user'] )
            ->where( 'agent_id', auth()->user()->id )
            ->where( 'state', 'COMPLETED' )
            ->latest()
            ->paginate();
    }

    public static function money()
    {
        $income   = Transection::where( 'agent_id', auth()->user()->id )->sum( 'commistion' );
        $withdraw = Transection::where( 'agent_id', auth()->user()->id )
            ->where( 'state', 'COMPLETED' )
            ->sum( 'withdraw' );

        return $income - $withdraw;
    }

    public static function withdraw()
    {
        return Transection::where( 'agent_id', auth()->user()->id )
            ->where( 'type', 'WITHDRAW' )
            ->where( 'state', 'PROCEED' )
            ->latest()
            ->first();
    }

    public static function store( string $event = '', string $type, $agent = '', float $price, $order = '', $user = '' )
    {
        if ( $agent ) {
            $data             = [];
            $data['agent_id'] = $agent;
            if ( $type === 'COMMISTION' ) {
                if ( $event === 'TOUR' ) {
                    $data['order_id']   = $order;
                    $data['title']      = 'ค่าคอมขายทัวร์ 10% ';
                    $data['commistion'] = max(  ( 10 * $price ) / 100, 0 );

                    $agentTwo = User::with( 'agent' )->find( $agent );
                    if ( $agentTwo->agent ) {
                        Transection::create( [
                            'agent_id'         => $agentTwo->agent->id,
                            'user_id'          => $user,
                            'commistion'       => max(  ( 5 * $price ) / 100, 0 ),
                            'title'            => 'ค่าคอมขายทัวร์ 5% ',
                            'type'             => $type,
                            'transection_type' => $event,
                        ] );
                    }
                } elseif ( $event === 'REGISTER' ) {
                    $data['title']      = 'ค่าส่วนแบ่งค่าสมัครสมาชิก 20% ';
                    $data['user_id']    = $user;
                    $data['commistion'] = max(  ( 20 * $price ) / 100, 0 );

                    $agentTwo = User::with( 'agent' )->find( $agent );
                    if ( $agentTwo->agent ) {
                        Transection::create( [
                            'agent_id'         => $agentTwo->agent->id,
                            'user_id'          => $user,
                            'commistion'       => max(  ( 5 * $price ) / 100, 0 ),
                            'title'            => 'ค่าส่วนแบ่งค่าสมัครสมาชิก 5% ',
                            'type'             => $type,
                            'transection_type' => $event,
                        ] );
                    }
                }
                $data['state']            = 'COMPLETED';
                $data['transection_type'] = $event;
            } elseif ( $type === 'WITHDRAW' ) {
                $data['withdraw'] = $price;
                $data['title']    = 'ถอนเงิน';
                $data['state']    = 'PROCEED';
            }
            $data['type'] = $type;
            Transection::create( $data );
        }
    }

    public function changeStatus( Request $request, $staus, Transection $id )
    {
        if ( $staus === 'confirm' ) {
            $validator = Validator::make( $request->all(), [
                'slip' => 'required|nullable|image',
            ] );

            if ( $validator->fails() ) {

                return back()->with( [
                    'message'    => 'กรุณากรอกหลักฐานการโอนเงิน',
                    'alert-type' => 'error',
                ] );

            }

            $id->slip         = $request->hasFile( 'slip' ) ? $request->file( 'slip' )->storeAs( 'slip', Str::random( 40 ) . '.jpg', 'public' ) : null;
            $id->state        = 'COMPLETED';
            $id->completed_at = Carbon::now()->toDateTimeString();
            $id->save();

            return back()->with( [
                'message'    => 'ยอมรับรายการถอนเงินเรียบร้อย',
                'alert-type' => 'success',
            ] );

        } elseif ( $staus === 'reject' ) {
            $id->state       = 'REJECTED';
            $id->rejected_at = Carbon::now()->toDateTimeString();
            $id->save();

            return back()->with( [
                'message'    => 'ยกเลิกรายการถอนเงินเรียบร้อย',
                'alert-type' => 'success',
            ] );
        }

    }
}
