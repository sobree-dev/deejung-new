<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;


class CheckduptitleController extends Controller
{
    public function checkDupTitle(Request $request)
    {

        $db    = $request->_db;
        $id    = $request->form_id;
        $value = $request->title_page;
        $field = $request->field;

        if (empty($id)) {
            $dup = DB::table($db)->where($field, $value)->count();
        } else {
            $dup = DB::table($db)->where($field, $value)->where('id', '!=', $id)->count();
        }

        return json_decode($dup);
    }

    public function checkDupSlug(Request $request)
    {

        $db    = $request->_db;
        $id    = $request->form_id;
        $value = $request->slug;
        $field = $request->field;

        if (empty($id)) {
            $dup = DB::table($db)->where($field, $value)->count();
        } else {
            $dup = DB::table($db)->where($field, $value)->where('id', '!=', $id)->count();
        }

        return json_decode($dup);
    }

    public function test(Request $request)
    {
        dd("AA");
    }
}
