<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Insurance extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name'    => 'required|min:3|max:255',
            'surname' => 'required|min:3|max:255',
            'phone'   => 'required|numeric|digits:10|regex:/^0[14689][0-9]{8}/',
            'card_id' => 'required|numeric|regex:/^([0-9]{13})$/',
        ];

        if ( $this->request->get( 'order_type' ) == 2 ) {

            $rules['insurance']      = 'required';
            $rules['insurance_type'] = 'required';
            $rules['origin']         = 'required';
            $rules['destination']    = 'required';
            $rules['date_start']     = 'required';
            $rules['date_end']       = 'required';
        }

        foreach ( $this->request->get( 'orders' ) as $key => $order ) {
            foreach ( $order as $keyItem => $val ) {
                $rules['orders.' . $key . '.tour_id'] = 'required|numeric|exists:App\Tour,id'; // check Tour id
                $rules['orders.' . $key . '.amount']  = 'required|numeric|min:1'; // check amount

                if ( intval( $order['price'] ) > 0 ) {
                    $rules['orders.' . $key . '.price'] = 'required|numeric|min:1'; // check price

                    $rules['payment_type'] = ['required', Rule::in( ['TRANSFER', 'PAYPAL'] )];
                    $rules['slip']         = 'required_if:payment_type,TRANSFER|nullable|image';
                }

            }
        }

        return $rules;
    }

    public function attributes()
    {
        return [
            'name'           => 'ชื่อ',
            'surname'        => 'นามสกุล',
            'phone'          => 'เบอร์โทรศัพท์',
            'card_id'        => 'เลขบัตรประชาชน/หนังสือเดินทาง',
            'insurance'      => 'ประกันภัย',
            'insurance_type' => 'ประเภทประกันภัย',
            'origin'         => 'ต้นทาง',
            'destination'    => 'ปลายทาง',
            'date_start'     => 'วันเริ่มต้น',
            'date_end'       => 'วันสิ้นสุด',
            'payment_type'   => 'ประเภทการชำระเงิน',
            'slip'           => 'สลิป',
        ];
    }
}
