<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;


class TaggableTag extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['*'];

    protected $primaryKey = 'tag_id';

    /**
     * Log
     */
    protected static $logName = 'Tag';

    protected static $logAttributes = ['*'];

    protected static $logOnlyDirty = true;

    protected static $submitEmptyLogs = false;

    public function getDescriptionForEvent(string $eventName): string
    {
        return "This model Tag to {$eventName}";
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeTagslug(Builder $query, $name)
    {
        return $query->join('taggable_taggables', 'taggable_tags.tag_id', '=', 'taggable_taggables.tag_id')
            ->join('posts', 'posts.id', '=', 'taggable_taggables.taggable_id')
            ->where('taggable_tags.name', $name);
    }
}
