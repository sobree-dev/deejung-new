<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    //

    protected $fillable = ['order_id', 'tour_id', 'amount', 'price', 'price_summary'];

    public function tour()
    {
        return $this->hasOne( 'App\Tour', 'id', 'tour_id' );
    }
}
